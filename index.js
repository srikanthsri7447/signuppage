const headerEl = document.getElementById('header-element')
const sectionElement  = document.getElementById('sectionElement')
const formEl = document.getElementById('formElement')
const firstNameInput = document.getElementById('first-name')
const lastNameInput = document.getElementById('last-name')
const emailInput = document.getElementById('email')
const passwordInput = document.getElementById('password')
const cnfrmPasswordInput = document.getElementById('confirmPassword')
const checkboxEl = document.getElementById('checkbox')
const firstErrEl = document.getElementById('firstErrTxt')
const lastErrEl = document.getElementById('lastErrTxt')
const emailErrEl = document.getElementById('emilErrTxt')
const passErrEl = document.getElementById('passErrTxt')
const cnfrmPassErrEl = document.getElementById('cnfrmPassErrTxt')
const checkboxErrEl = document.getElementById('checkboxErrTxt')
const profileEl = document.getElementById('profile')

window.onload =()=>{
    firstNameInput.value = ''
    lastNameInput.value = ''
    emailInput.value = ''
    passwordInput.value = ''
    cnfrmPasswordInput.value = ''
    checkboxEl.checked = false

}


function ValidateName(name) {
    const nameformat = /^[a-zA-Z]+$/

    if (name.match(nameformat)) {
        return true
    }
    else {
        return false
    }
}

function ValidateEmail(mail) {

    const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/

    if (mail.match(mailformat)) {
        return true
    }
    else {
        return (false)
    }
}

function CheckPassword(password) {

    const decimal = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;

    if (password.match(decimal)) {
        return true;
    }
    else {
        return false;
    }
}

let isFirstName;
let isLastName;
let isEmail;
let isPassword;
let isCnfrmPassword;
let isChecked;


firstNameInput.addEventListener('blur', (event) => {
    event.target.value = event.target.value.trim()
    if (event.target.value === '') {

        firstErrEl.textContent = '*Require First Name'
    }
    else {

        if (ValidateName(event.target.value)) {
            firstErrEl.textContent = ''
            isFirstName = true;

        } else {
            firstErrEl.textContent = '*First name must be letters'
        }

    }
})

lastNameInput.addEventListener('blur', (event) => {
    event.target.value = event.target.value.trim();
    if (event.target.value === '') {
        lastErrEl.textContent = '*Require Last Name'
    }
    else {
        if (ValidateName(event.target.value)) {

            lastErrEl.textContent = ''
            isLastName = true;

        }
        else {
            lastErrEl.textContent = 'Last name must be letters'
        }
    }
})

emailInput.addEventListener('blur', (event) => {

    if (event.target.value === '') {
        emailErrEl.textContent = '*Require Email'
    }
    else {

        if (ValidateEmail(event.target.value)) {
            emailErrEl.textContent = ''
            isEmail = true
        }
        else {
            emailErrEl.textContent = '*Enter valid Email Address'
        }
    }
})

passwordInput.addEventListener('blur', (event) => {
    if (event.target.value === '') {
        passErrEl.textContent = '*Require Password'
    }
    else {
        if (event.target.value.length >= 8) {
            if (CheckPassword(event.target.value)) {
                passErrEl.textContent = ''
                isPassword = true;
            }
            else {
                passErrEl.textContent = 'Password should between 8 to 15 characters, which contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character'
            }
        }
        else {
            passErrEl.textContent = 'Password should between 8 to 15 characters, which contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character'

        }
    }

})

cnfrmPasswordInput.addEventListener('blur', (event) => {
    if (event.target.value === '') {
        cnfrmPassErrEl.textContent = '*Re-enter Password'
    }
    else {
        cnfrmPassErrEl.textContent = ''
        let isPasswordMatched = (passwordInput.value === event.target.value)

        if (isPasswordMatched) {
            cnfrmPassErrEl.textContent = ''
            isCnfrmPassword = true;
        }
        else {
            cnfrmPassErrEl.textContent = 'Password Not Matched'
        }
    }
})

checkboxEl.addEventListener('change', () => {
    if (checkboxEl.checked) {
        checkboxErrEl.textContent = ''
        isChecked = true;
    }
    else {
        checkboxErrEl.textContent = 'Please agree Terms & Conditions'
    }
})


const validateData = () => {

    if (firstNameInput.value === '') {

        firstErrEl.textContent = '*Require First Name'
    }

    if (lastNameInput.value === '') {
        lastErrEl.textContent = '*Require Last Name'
    }

    if (emailInput.value === '') {
        emailErrEl.textContent = '*Require Email'
    }
    

    if (passwordInput.value === '') {
        passErrEl.textContent = '*Require Password'
    }
    

    if (cnfrmPasswordInput.value === '') {
        cnfrmPassErrEl.textContent = '*Re-enter Password'
    }
   

    if (!checkboxEl.checked) {
        checkboxErrEl.textContent = '*Please agree Terms & Conditions'
    }
    

    if (isFirstName && isLastName && isEmail && isPassword && isCnfrmPassword && isChecked) {
        
        firstNameInput.value = ''
        lastNameInput.value = ''
        emailInput.value = ''
        passwordInput.value = ''
        cnfrmPasswordInput.value = ''
        checkboxEl.checked = false

        isFirstName = false;
        isLastName = false;
        isEmail = false;
        isPassword = false;
        isCnfrmPassword = false;
        isChecked = false;

        setTimeout(() => {
            
            formEl.classList.add('hideForm')

            headerEl.textContent = 'Profile Created Successfully!'
            headerEl.classList.add('green-back')

            profileEl.classList.add('displayImg')

            sectionElement.classList.add('marginEl')

        }, 1000)
    }


}


function onSubmit() {

    validateData()
}